import Vue from 'vue';
import App from './App.vue';
import router from './router';
import vuetify from './plugins/vuetify'
import store from './store';
import Toast from "vue-toastification";
import VueMoment from 'vue-moment'
import VueGoogleCharts from 'vue-google-charts';

// Import the CSS or use your own!
import "vue-toastification/dist/index.css";

Vue.use(Toast);
Vue.use(VueMoment);
Vue.use(VueGoogleCharts);

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
