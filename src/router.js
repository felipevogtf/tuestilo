import Vue from 'vue'
import VueRouter from 'vue-router'
import PropietarioRoutes from "./pages/propietario/routes"
import ClienteRoutes from "./pages/cliente/routes"
import Store from "./store/index"


Vue.use(VueRouter)
/*Rutas para acceder a las paginas indicando el componente que contiene el contenido html, css, y js*/
const routes = [
    {
        path: '/',
        name: 'Inicio',
        component: () => import('./pages/Inicio.vue'),
    },
    {
        path: '/login',
        name: 'Login',
        component: () => import('./pages/user/Login.vue'),
    },
    {
        path: '/registro',
        name: 'Register',
        component: () => import('./pages/user/Register.vue'),
    },
    {
        path: '/propietario',
        name: 'PropietarioPage',
        component: () => import('./pages/propietario/PropietarioPage.vue'),
        redirect: { name: 'Propietario.Inicio' },
        children: PropietarioRoutes,
        meta: {
            requiresAuth: true,
            adminAuth: true,
        }
    },
    {
        path: '/admin',
        name: 'Admin',
        component: () => import('./pages/admin/Admin.vue'),
        meta: {
            requiresAuth: true,
            superAuth: true,
        }
    },
    {
        path: '/cliente',
        name: 'ClientePage',
        component: () => import('./pages/cliente/ClientePage.vue'),
        redirect: { name: 'Cliente.Inicio' },
        children: ClienteRoutes,
    },
    { path: '*', redirect: '/' }
]

const router = new VueRouter({
    mode: 'history',
    routes
})

router.beforeEach((to, from, next) => {
    const authRequired = to.matched.some(m => m.meta.requiresAuth);
    const adminRequired = to.matched.some(m => m.meta.adminAuth);
    const superAuth = to.matched.some(m => m.meta.superAuth);
    const user = Store.state.auth.user;

    if (authRequired) {
        if (!user) {
            next({ name: 'Login' });
        }

        if (adminRequired && user) {
            if (user.user.type == 1 || user.user.type == 3) {
                next();
            } else {
                next({ name: 'Login' });
            }
        }
        if (superAuth && user) {
            if (user.user.type == 4) {
                next();
            } else {
                next({ name: 'Login' });
            }
        }

    }
    next();
});


export default router
