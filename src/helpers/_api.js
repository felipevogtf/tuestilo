export const API_URL = 'http://127.0.0.1:8000/api/';
export const API_HEADER_FORM = {
    headers: {
        'Content-Type': 'multipart/form-data'
    }
};

export function API_IMG(img) {
    return "http://127.0.0.1:8000/storage/" + img;
}
