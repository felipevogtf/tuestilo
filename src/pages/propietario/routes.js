export default [
    {
        path: '/',
        name: 'Propietario.Inicio',
        component: () => import('./pages/Inicio.vue')
    },
    {
        path: '/Establecimiento',
        name: 'Propietario.Establecimiento',
        component: () => import('./pages/Establecimiento.vue')
    },
    {
        path: '/Materiales',
        name: 'Propietario.Materiales',
        component: () => import('./pages/Materiales.vue')
    },
    {
        path: '/Servicios',
        name: 'Propietario.Servicios',
        component: () => import('./pages/Servicios.vue')
    },
    {
        path: '/Personal',
        name: 'Propietario.Personal',
        component: () => import('./pages/Personal.vue')
    },
    {
        path: '/Ventas',
        name: 'Propietario.Ventas',
        component: () => import('./pages/Ventas.vue')
    },
    {
        path: '/Cuenta',
        name: 'Propietario.Cuenta',
        component: () => import('./pages/Cuenta.vue')
    },
    {
        path: '/Planes',
        name: 'Propietario.Planes',
        component: () => import('./pages/Planes.vue')
    },
    {
        path: '/Reservas',
        name: 'Propietario.Reservas',
        component: () => import('./pages/Reservas.vue')
    },
]
