export default [
    {
        path: '/tiendas',
        name: 'Cliente.Tiendas',
        component: () => import('./pages/Tiendas.vue')
    },
    {
        path: '/inicio',
        name: 'Cliente.Inicio',
        component: () => import('./pages/Inicio.vue')
    },
    {
        path: '/tiendas/:idTienda',
        name: 'Cliente.VerTienda',
        component: () => import('./pages/VerTienda.vue')
    },
    {
        path: '/tiendas/:idTienda/catalogo',
        name: 'Cliente.CatalogoTienda',
        component: () => import('./pages/CatalogoTienda.vue')
    },
    {
        path: '/reservas',
        name: 'Cliente.Reservas',
        component: () => import('./pages/Reservas.vue')
    },
    {
        path: '/perfil',
        name: 'Cliente.Perfil',
        component: () => import('./pages/Perfil.vue')
    }
]

