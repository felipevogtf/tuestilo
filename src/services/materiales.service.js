import axios from 'axios';
import { API_URL } from '../helpers/_api'


class MaterialesService {
    store(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/materiales', data)
            .then(response => {
                return response.data;
            });
    }

    update(data, idMaterial) {
        return axios
            .put(API_URL + 'materiales/' + idMaterial, data)
            .then(response => {
                return response.data;
            });
    }

    delete(idMaterial) {
        return axios
            .delete(API_URL + 'materiales/' + idMaterial)
            .then(response => {
                return response.data;
            });
    }
    getMateriales(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/materiales')
            .then(response => {
                return response.data;
            });
    }

    getCategorias() {
        return axios
            .get(API_URL + 'categorias_materiales')
            .then(response => {
                return response.data;
            });
    }
}

export default new MaterialesService();
