import axios from 'axios';
import { API_URL, API_HEADER_FORM } from '../helpers/_api'

class EstablecimientoService {
    store(data) {
        return axios
            .post(API_URL + 'establecimiento', data, API_HEADER_FORM)
            .then(response => {
                return response.data;
            });
    }

    update(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id, data)
            .then(response => {
                return response.data;
            });
    }
    getEstablecimiento(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id,)
            .then(response => {
                return response.data;
            });
    }

    estadisticas(id, data) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/estadisticas', data)
            .then(response => {
                return response.data;
            });
    }

    getEstablecimientos() {
        return axios
            .get(API_URL + 'establecimientos/',)
            .then(response => {
                return response.data;
            });
    }
    getTiposEstablecimientos() {
        return axios
            .get(API_URL + 'tipos_establecimientos')
            .then(response => {
                return response.data;
            });
    }

    planes() {
        return axios
            .get(API_URL + 'planes')
            .then(response => {
                return response.data;
            });
    }

    solicitarPlan(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/solicitud', data)
            .then(response => {
                return response.data;
            });
    }
}

export default new EstablecimientoService();
