import axios from 'axios';
import { API_URL } from '../helpers/_api'


class VentasServices {
    store(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/ventas', data)
            .then(response => {
                return response.data;
            });
    }

    update(data, idVenta) {
        return axios
            .put(API_URL + 'ventas/' + idVenta, data)
            .then(response => {
                return response.data;
            });
    }

    delete(idVenta) {
        return axios
            .delete(API_URL + 'ventas/' + idVenta)
            .then(response => {
                return response.data;
            });
    }
    getVentas(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/ventas')
            .then(response => {
                return response.data;
            });
    }
}

export default new VentasServices();
