import axios from 'axios';
import { API_URL } from '../helpers/_api'


class PuntuacionService {
    store(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/puntuacion', data)
            .then(response => {
                return response.data;
            });
    }
}

export default new PuntuacionService();
