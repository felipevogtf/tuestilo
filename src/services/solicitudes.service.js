import axios from 'axios';
import { API_URL } from '../helpers/_api'


class SolicitudesService {
    getSolicitudes() {
        return axios
            .get(API_URL + 'solicitudes/')
            .then(response => {
                return response.data;
            });
    }

    aprobar(id) {
        return axios
            .post(API_URL + 'solicitud/' + id)
            .then(response => {
                return response.data;
            });
    }

    rechazar(id) {
        return axios
            .delete(API_URL + 'solicitud/' + id)
            .then(response => {
                return response.data;
            });
    }

    desactivarEstablecimiento(id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/desactivar')
            .then(response => {
                return response.data;
            });
    }

    vencidosEstablecimientos() {
        return axios
            .get(API_URL + 'establecimientos/vencidos/')
            .then(response => {
                return response.data;
            });
    }

    desactivarEstablecimientoPremium(id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/desactivarPremium')
            .then(response => {
                return response.data;
            });
    }

    vencidosEstablecimientoPremium() {
        return axios
            .get(API_URL + 'establecimientos/vencidosPremium/')
            .then(response => {
                return response.data;
            });
    }
}

export default new SolicitudesService();
