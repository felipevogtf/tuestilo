import axios from 'axios';
import { API_URL } from '../helpers/_api'


class PersonalService {
    store(data, id) {
        return axios
            .post(API_URL + 'establecimiento/' + id + '/personal', data)
            .then(response => {
                return response.data;
            });
    }

    update(data, idPersonal) {
        return axios
            .put(API_URL + 'personal/' + idPersonal, data)
            .then(response => {
                return response.data;
            });
    }

    delete(idPersonal) {
        return axios
            .delete(API_URL + 'personal/' + idPersonal)
            .then(response => {
                return response.data;
            });
    }
    getPersonal(id) {
        return axios
            .get(API_URL + 'establecimiento/' + id + '/personal')
            .then(response => {
                return response.data;
            });
    }
}

export default new PersonalService();
